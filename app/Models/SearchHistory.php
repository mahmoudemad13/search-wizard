<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method create(array $all)
 * @method where(string $string, string $string1, string $string2)
 * @method whereKeyWord($q)
 */
class SearchHistory extends  Model
{

    protected $table = "search_history";

    protected $fillable = ["keyword","count_as_part","count_as_whole"];
}
