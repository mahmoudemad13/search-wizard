<?php

namespace App\Services;
use jcobhams\NewsApi\NewsApi;


class NewsApiService
{

    private $newsApi;


    public function __construct()
    {
        $this->newsApi = new NewsApi(env("NEWSAPI_KEY"));
    }

    public function searchInEverything($q, $sources, $domains, $exclude_domains, $from, $to, $language, $sort_by,  $page_size, $page)
    {
        return $this->newsApi->getEverything($q, $sources, $domains, $exclude_domains, $from, $to, $language, $sort_by,  $page_size, $page);
    }
}
