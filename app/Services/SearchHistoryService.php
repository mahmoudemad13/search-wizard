<?php

namespace App\Services;


use App\Models\SearchHistory;

class SearchHistoryService
{

    private $searchHistoryProxy;


    public function __construct(SearchHistory $searchHistory)
    {
        $this->searchHistoryProxy = $searchHistory;
    }


    /**
      * according to functional programming technique avoiding if/else & for loops in order to have best performance level.
      * logic trace:-
      *   if searched text is more than one keyword then check on every keyword
      *     if keyword exist then increment count_as_part by 1
      *     else insert new one with count_as_part = 1,
      *   else searched text has one keyword then check
      *     if exist then increment count_as_whole by 1
      *     else insert new one with count_as_whole = 1.
    **/
    public function logSearch()
    {
        count(explode(" ",request()->q)) > 1 ?
            array_map(function($keyword){
                $this->getRecordByKeyWord($keyword) ?
                    $this->incrementCountAsPart($keyword)
                :   $this->insert($keyword,"count_as_part");
            },explode(" ",request()->q))
        :  ($this->getRecordByKeyWord(request()->q) ?
                $this->incrementCountAsWhole(request()->q)
            :   $this->insert(request()->q,"count_as_whole"));

    }

    public function insert($keyword,$count_type)
    {
     $this->searchHistoryProxy->create(["keyword" => $keyword,$count_type => 1]);
    }

    public function incrementCountAsPart($keyword)
    {
        $this->getRecordByKeyWord($keyword)->increment("count_as_part");
    }

    public function incrementCountAsWhole($keyword)
    {
        $this->getRecordByKeyWord($keyword)->increment("count_as_whole");
    }

    public function getRecordByKeyWord($keyword)
    {
        return $this->searchHistoryProxy->whereKeyword($keyword)->first();
    }


}
