<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Services\NewsApiService;
use App\Services\SearchHistoryService;


class NewsApiController extends Controller
{
    private $newsApiService;
    private $searchHistoryService;

    public function __construct(NewsApiService $newsApiService,SearchHistoryService $searchHistoryService,SearchRequest $searchRequest)
    {
        $this->newsApiService = $newsApiService;
        $this->searchHistoryService = $searchHistoryService;
    }

    public function search()
    {
        request()->page > 1 ? null : $this->logSearch() ;

        return $this->newsApiService->searchInEverything(
            request()->q,
            request()->sources,
            request()->domains,
            request()->exclude_domains,
            request()->from,
            request()->to,
            request()->language,
            request()->sort_by,
            request()->page_size,
            request()->page);
    }

    public function logSearch()
    {
        $this->searchHistoryService->logSearch();
    }


}
